#!/bin/bash

_TEST_STR="Hello World!"
_phpres="$(php -r '$x = "Hello World!"; echo "$x\n";')"
printf "Testing a 'Hello World!' program on an interactive shell ..."
if [[ "${_phpres}" != "${_TEST_STR}" ]]; then
	printf "FAILED!\n"
	exit 1
fi
printf "PASSED\n"
