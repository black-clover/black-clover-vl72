#!/bin/bash

# Use curl to test an url

printf "Testing 'curl' program ... "
curl www.google.com > /dev/null 2>&1
if [ $? -gt 0 ]; then
	printf "FAILED!\n"
else
	printf "PASSED\n"
fi
