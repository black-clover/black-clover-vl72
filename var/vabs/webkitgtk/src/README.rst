WEBKITGTK
=========

This directory holds the build sources for webkitgtk.


WARNING
=======

This will build the webkitgtk version 2.10.x (api version 4) of the
webkitgtk project.  Some older applications will not build against 
this version of webkitgtk.  For those, use one of ``webkitgtk2`` or
``webkitgtk3``

