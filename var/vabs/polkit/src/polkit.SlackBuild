#!/bin/sh

# Copyright 2009, 2011  Robby Workman, Northport, Alabama, USA
# Copyright 2010  Eric Hameleers, Eindhoven, NL
# Copyright 2009, 2010, 2011, 2012  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.

# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME="polkit"
VERSION=${VERSION:-"0.113"}
BUILDNUM=${BUILDNUM:-"1"}
ARCH=${ARCH:-$(uname -m)}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://freedesktop.org/software/$NAME/releases/$NAME-$VERSION.tar.gz"}
MAKEDEPENDS=${MAKEDEPENDS:-"eggdbus js185 expat gobject-introspection \
	vim \
	!nspr mozilla-nss \
	p11-kit"}
NUMJOBS=${NUMJOBS:--j6}

if [ "$NORUN" != 1 ]; then
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
  CONFIGURE_TRIPLET="i586-vector-linux"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf $NAME-$VERSION
tar xf $CWD/$NAME-$VERSION.tar.?z* || exit 1
cd $NAME-$VERSION || exit 1

# Make sure ownerships and permissions are sane:
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --docdir=/usr/doc/$NAME-$VERSION \
  --enable-man-pages \
  --enable-gtk-doc \
  --mandir=/usr/man \
  --disable-static \
  --enable-introspection \
  --with-authfw=shadow \
  --enable-verbose-mode \
  --with-os-type=Slackware \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1
#  --build=$ARCH-slackware-linux || exit 1

#NOTE: The directory /etc/polkit-1/localauthority must be owned
#      by root and have mode 700
#NOTE: The directory /var/lib/polkit-1 must be owned
#      by root and have mode 700
#NOTE: The file ${exec_prefix}/libexec/polkit-agent-helper-1 must be owned
#      by root and have mode 4755 (setuid root binary)
#NOTE: The file ${exec_prefix}/bin/pkexec must be owned by root and
#      have mode 4755 (setuid root binary)

# Build and install:
make $NUMJOBS || exit 1
make install DESTDIR=$PKG || exit 1

# Add polkit home dir
mkdir -p $PKG/var/lib/polkit

# Add default policy files for udisks2 and NetworkManager events:
mkdir -p $PKG/etc/polkit-1/localauthority/50-local.d
cat $CWD/20-plugdev-group-mount-override.pkla > $PKG/etc/polkit-1/localauthority/50-local.d/20-plugdev-group-mount-override.pkla.new
cat $CWD/10-org.freedesktop.NetworkManager.pkla > $PKG/etc/polkit-1/localauthority/50-local.d/10-org.freedesktop.NetworkManager.pkla.new

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.*
      )
    done
  )
fi

# Add a documentation directory:
mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a \
  AUTHORS COPYING HACKING INSTALL NEWS README \
  $PKG/usr/doc/$NAME-$VERSION
( cd $PKG/usr/doc/$NAME-$VERSION; ln -s ../../share/gtk-doc/html/polkit-1 html )

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/*-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
zcat $CWD/doinst.sh > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz
rm -rf $TMP
fi
