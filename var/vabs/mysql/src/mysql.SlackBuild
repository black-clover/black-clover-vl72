#!/bin/bash

# Copyright 2008, 2009, 2010  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Build and install MySQL on Slackware
# by:  David Cantrell <david@slackware.com>
# Currently maintained by:  Patrick Volkerding <volkerdi@slackware.com>

NAME="mysql"
VERSION=${VERSION:-"5.7.18"}
BASEVER=$(echo $VERSION | cut -f 1-2 -d .)
ARCH=${ARCH:-$(uname -m)}
BUILDNUM=${BUILDNUM:-1}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"http://dev.mysql.com/get/Downloads/MySQL-${VERSION%.*}/mysql-${VERSION}.tar.gz"}

MAKEDEPENDS="slapt-get openssl curl zlib tcp_wrappers boost "
NUMJOBS=${NUMJOBS:-"-j6"}
CWD=$(pwd)
TMP=${TMP:-$CWD../tmp}
OUTPUT=${OUTPUT:-${CWD}/..}
PKG=$TMP/package-mysql

if [ "$NORUN" != 1 ]; then 	# Do not execute if NORUN is set to 1

# Download the source code
for link in $(echo $LINK); do
	(
	cd $CWD
	wget -c --no-check-certificate $link
	)
done


if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf mysql-$VERSION
tar xf $CWD/mysql-$VERSION.tar.?z* --checkpoint=10000 || exit 1
cd mysql-$VERSION

if ls $CWD/*.diff.gz 1> /dev/null 2> /dev/null ; then
  for patch in $CWD/*.diff.gz ; do
    zcat $patch | patch -p1 --verbose || exit 1
  done
fi

patch -Np1 -i $CWD/patches/mysql-5.6.16-embedded_library_shared-1.patch --verbose || exit 1

chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

sed -i "/ADD_SUBDIRECTORY(sql\/share)/d" CMakeLists.txt &&
sed -i "s/ADD_SUBDIRECTORY(libmysql)/&\\nADD_SUBDIRECTORY(sql\/share)/" CMakeLists.txt
sed -i "s@data/test@\${INSTALL_MYSQLSHAREDIR}@g" sql/CMakeLists.txt
sed -i "s@data/mysql@\${INSTALL_MYSQLTESTDIR}@g" sql/CMakeLists.txt

sed -i "s/srv_buf_size/srv_sort_buf_size/" storage/innobase/row/row0log.cc

(mkdir source_downloads
cd source_downloads
#wget -c --no-check-certificate http://pkgs.fedoraproject.org/repo/pkgs/gmock/gmock-1.6.0.zip/f547f47321ca88d3965ca2efdcc2a3c1/gmock-1.6.0.zip || exit 1
wget -c --no-check-certificate https://github.com/google/googlemock/archive/release-1.6.0.zip

unzip release-1.6.0.zip || exit 1
mv googlemock* gmock-1.6.0
rm release-1.6.0.zip) || exit 1

mkdir build
cd build
cmake \
 -DCMAKE_BUILD_TYPE=Release \
 -DCMAKE_INSTALL_PREFIX=/usr \
 -DCMAKE_INCLUDE_PATH=/usr/include \
 -DCMAKE_LIBRARY_PATH=/usr/lib${LIBDIRSUFFIX} \
 -DMYSQL_DATADIR=/var/lib/mysql \
 -DSYSCONFDIR=/etc/mysql \
 -DINSTALL_LIBDIR=lib${LIBDIRSUFFIX}/mysql \
 -DINSTALL_INFODIR=share/mysql/docs \
 -DINSTALL_MANDIR=share/man \
 -DINSTALL_PLUGINDIR=lib${LIBDIRSUFFIX}/mysql/plugin \
 -DINSTALL_SCRIPTDIR=bin \
 -DINSTALL_INCLUDEDIR=include/mysql \
 -DINSTALL_DOCREADMEDIR=share/mysql \
 -DINSTALL_SUPPORTFILESDIR=share/mysql \
 -DINSTALL_MYSQLSHAREDIR=share/mysql \
 -DINSTALL_MYSQLDATADIR=/var/lib/mysql \
 -DINSTALL_SHAREDIR=share/mysql \
 -DWITH_READLINE=ON \
 -DWITH_ZLIB=system \
 -DWITH_SSL=system \
 -DWITH_LIBWRAP=ON \
 -DDEFAULT_CHARSET=utf8 \
 -DDEFAULT_COLLATION=utf8_general_ci \
 -DWITH_EXTRA_CHARSETS=complex \
 -DWITH_EMBEDDED_SERVER=ON \
 -DWITH_MYSQLD_LDFLAGS="${LDFLAGS}" \
 -DMYSQL_UNIX_ADDR=/var/run/mysqld/mysqld.sock \
 -DENABLED_LOCAL_INFILE=ON \
 -DWITH_PARTITION_STORAGE_ENGINE=1 \
 -DWITHOUT_EXAMPLE_STORAGE_ENGINE=1 \
 -DWITHOUT_ARCHIVE_STORAGE_ENGINE=1 \
 -DWITHOUT_BLACKHOLE_STORAGE_ENGINE=1 \
 -DWITHOUT_FEDERATED_STORAGE_ENGINE=1 \
 -DWITH_INNOBASE_STORAGE_ENGINE=1 .. || exit 1

make $NUMJOBS || exit 1
DESTDIR=$PKG make install || exit 1

cd ..


# Install default conf file
mkdir -p $PKG/etc/mysql
zcat $CWD/my.cnf.gz > $PKG/etc/mysql/my.cnf || exit 1


# Make the default place for the databases
install -v -m0755 -o mysql -g mysql -d $PKG/run/mysqld
mkdir -p $PKG/var/lib/mysql

# Prepare post-install script to initialize installation
mkdir -p $PKG/install
cat > $PKG/install/doinst.sh << FINO
groupadd -g 40 mysql
useradd -c "MySQL Server" -d /var/lib/mysql -g mysql -s /bin/false -u 40 mysql
mysql_install_db --basedir=/usr --datadir=/var/lib/mysql --user=mysql >/dev/null 2>&1
chown -R mysql:mysql /var/lib/mysql

FINO

# install additional headers needed for building external engine plugins
for i in sql include regex; do
  for j in $i/*.h; do
    install -m 644 $j $PKG/usr/include/mysql/
  done
done

mkdir -p $PKG/usr/include/mysql/atomic
for i in include/atomic/*.h; do
  install -m 644 $i $PKG/usr/include/mysql/atomic/
done

# The ./configure option to omit this has gone away, so we'll omit it
# the old-fashioned way.  It's all in the source tarball if you need it.
rm -rf $PKG/usr/sql-bench

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Install support files
#mkdir -p $PKG/etc
#cp support-files/my-{huge,large,medium,small}.cnf.sh $PKG/etc

# Install docs
mkdir -p $PKG/usr/doc/mysql-$VERSION/Docs

cp -a \
  COPYING* README* \
  $PKG/usr/doc/mysql-$VERSION
( cd Docs
  # Seems most of the Docs/* are gone, but well leave the cp stuff
  # in case it returns.
  cp -a *.txt \
     $PKG/usr/doc/mysql-$VERSION/Docs )
## Too large to justify since the .html version is right there:
#rm $PKG/usr/doc/mysql-$VERSION/Docs/manual.txt
find $PKG/usr/doc/mysql-$VERSION -type f -exec chmod 0644 {} \;

# This is the directory where databases are stored
mkdir -p $PKG/var/lib/mysql
chown mysql.mysql $PKG/var/lib/mysql
chmod 0750 $PKG/var/lib/mysql

# This is where the socket is stored
mkdir -p $PKG/var/run/mysql
chown mysql.mysql $PKG/var/run/mysql
chmod 0755 $PKG/var/run/mysql

# Do not include the test suite:
rm -rf $PKG/usr/mysql-test

# Add init script:
mkdir -p $PKG/etc/rc.d
# This is intentionally chmod 644.
zcat $CWD/rc.mysqld.gz > $PKG/etc/rc.d/rc.mysqld.new

# Install script:
mkdir -p $PKG/install
zcat $CWD/doinst.sh.gz >> $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

# Add some handy library symlinks:
if [ -r $PKG/usr/lib${LIBDIRSUFFIX}/mysql/libmysqlclient.so.?? ]; then
  ( cd $PKG/usr/lib${LIBDIRSUFFIX}
    rm -f libmysqlclient.so libmysqlclient.so.??
    ln -sf mysql/libmysqlclient.so .
    ln -sf mysql/libmysqlclient.so.?? .
  )
else
  echo "Could not find libmysqlclient.so.??"
  exit 1
fi

# Packaging standards:
rm -f $PKG/usr/info/dir

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

# Compress info files, if any:
if [ -d $PKG/usr/info ]; then
  ( cd $PKG/usr/info
    rm -f dir
    gzip -9 *
  )
fi

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/*-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

# Build package:
cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $OUTPUT $PKG
cat $CWD/slack-desc > $OUTPUT/slack-desc
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"

/sbin/makepkg -l y -c n $OUTPUT/$NAME-$VERSION-$ARCH-$BUILD.txz
fi
